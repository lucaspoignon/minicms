<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Auth;
use App\Content;

class WelcomeController extends Controller
{

    public function __construct()
    {
        //Je moet authenticated zijn voor alle methods behalve index
        $this->middleware('auth', ['except' => ['index']]);
    }

    public function curl($url) {
        // Assigning cURL options to an array
        $options = Array(
            CURLOPT_RETURNTRANSFER => TRUE,  // Setting cURL's option to return the webpage data
            CURLOPT_FOLLOWLOCATION => TRUE,  // Setting cURL to follow 'location' HTTP headers
            CURLOPT_AUTOREFERER => TRUE, // Automatically set the referer where following 'location' HTTP headers
            CURLOPT_CONNECTTIMEOUT => 120,   // Setting the amount of time (in seconds) before the request times out
            CURLOPT_TIMEOUT => 120,  // Setting the maximum amount of time for cURL to execute queries
            CURLOPT_MAXREDIRS => 10, // Setting the maximum number of redirections to follow
            CURLOPT_USERAGENT => "Mozilla/5.0 (X11; U; Linux i686; en-US; rv:1.9.1a2pre) Gecko/2008073000 Shredder/3.0a2pre ThunderBrowse/3.2.1.8",  // Setting the useragent
            CURLOPT_URL => $url, // Setting cURL's URL option with the $url variable passed into the function
        );
         
        $ch = curl_init();  // Initialising cURL 
        curl_setopt_array($ch, $options);   // Setting cURL's options using the previously assigned array data in $options
        $data = curl_exec($ch); // Executing the cURL request and assigning the returned data to the $data variable
        curl_close($ch);    // Closing cURL 
        return $data;   // Returning the data from the function 
    }

    public function scrape_between($data, $start, $end){
        $data = stristr($data, $start); // Stripping all data from before $start
        $data = substr($data, strlen($start));  // Stripping $start
        $stop = stripos($data, $end);   // Getting the position of the $end of the data to scrape
        $data = substr($data, 0, $stop);    // Stripping all data from after and including the $end of the data to scrape
        return $data;   // Returning the scraped data from the function
    }

    public function youtube_exists($videoID) {
        $theURL = "http://www.youtube.com/oembed?url=http://www.youtube.com/watch?v=$videoID&format=json";
        $headers = get_headers($theURL);

        if (substr($headers[0], 9, 3) !== "404") {
            return true;
        }
        else {
            return false;
        }
    }

    public function catch_that_image($url) {
        //$html = file_get_contents();
        libxml_use_internal_errors(true);
        $dom = new \DOMDocument();
        $dom->loadHTMLFile($url);
        $imgTags = $dom->getElementsByTagName('img');
        if ($imgTags->length > 0) {
            $imgElement = $imgTags->item(0);
            //return $imgElement->getAttribute('src');
            return $imgElement->getAttribute('src');
        } else {
            return false;
        }
    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $content = Content::all();
        //Get the SoundCloud URL
        return view('welcome', compact('content'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
       return view('content.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //Hier gekozen voor een korte validator in de controller omdat er maar één veld gecontroleerd wordt.
        $this->validate($request, [
        'url' => 'required|url|max:300',
        ]);

        $userId = Auth::id();
        $url = $request['url'];

        //De titel scrappen uit de webpagina van de URL.
        $scraped_website = $this->curl($url);
        $scraped_titel = $this->scrape_between($scraped_website, "<title>", "</title>");
        
        //Checken van welk type de URL is. Welke woorden komen voor in de URL?
        $arraymedia = array("youtube", "vimeo", "soundcloud");

        foreach ($arraymedia as $media) {
            if (strpos($url,$media) !== false) {
            $type = $media;
            }
            else {
            $type = "other";
            }
        }

        //De Embed URL samenstellen naargelang het type URL.
        switch ($type) {
            case 'youtube':
                $youtube_video_id = substr($url, -11);
                if ($this->youtube_exists($youtube_video_id)) {
                    $embed_url = "https://www.youtube.com/embed/".$youtube_video_id;
                }
                else {
                    return redirect('content/add')->with('added', 'Je hebt een verkeerde youtube URL ingegeven.');
                }
                break;
            case 'vimeo':
                $vimeo_video_id = substr($url, -9);
                $getValues = @file_get_contents('https://vimeo.com/api/oembed.json?url=https%3A//vimeo.com/'.$vimeo_video_id.'');
                if($getValues === FALSE) {
                    return redirect('content/add')->with('added', 'Je hebt een verkeerde Vimeo URL ingegeven.');
                }
                else {
                    $embed_url = "http://player.vimeo.com/video/".$vimeo_video_id;
                }
                //$embed_url = "http://player.vimeo.com/video/".$vimeo_video_id;
                break;
            case 'soundcloud':
                //Get the JSON data of song details with embed code from SoundCloud oEmbed
                //Gebruik hier @ om de error te supressen. Als de HTTP request mislukt omwille van een slechte URL,
                //returnt hij nu een false statement in plaats van een volledige error.
                $getValues = @file_get_contents('http://soundcloud.com/oembed?format=js&url='.$url.'&iframe=true');
                if($getValues === FALSE) {
                    return redirect('content/add')->with('added', 'Je hebt een verkeerde Soundcloud URL ingegeven.');
                }
                else {
                    //Clean the Json to decode
                    $decodeiFrame=substr($getValues, 1, -2);
                    //json decode to convert it as an array
                    $jsonObj = json_decode($decodeiFrame);
                    $srcHTML = $jsonObj->html; 
                    $embed_url = substr($srcHTML, strpos($srcHTML, "src") + 5, -11);
                }
                break;
            case 'other':
                //De eerste image tag zoeken door middel van de URL.
                $image = $this->catch_that_image($url);
                //Kijken of image een false waarde teruggeeft.
                if($image === FALSE){
                    //Er is geen image gevonden, standaard image gebruikt van de applicatie.
                    $embed_url = "/images/no_image.png/";
                }
                else {
                    //Controleren of de img src begint met een /. Returnt true of false.
                    $check = strpos($image, "/", 0);
                    if($check !== false) {
                        //De eerste character is /. Dit betekent dat de root van de website niet mee in de src zit.
                        //Die gaan we handmatig toevoegen.
                        $embed_url = $url.$image; 
                    }
                    else {
                        $embed_url = $image;
                    }
                }
                break;
        }
        
        $content = new Content;
        $content->url = $request['url'];
        $content->embed = $embed_url;
        $content->type = $type;
        $content->titel = $scraped_titel;
        $content->user_id = $userId;
        $content->save();

        restore_error_handler();

        return redirect('/')->with('added', 'Goodjob! Je hebt succesvol content toegevoegd.');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

}
