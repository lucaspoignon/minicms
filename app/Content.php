<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Content extends Model
{
        /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['url', 'type','titel'];

    public function User()
    {
        return $this->belongsTo('App\User');
    }

}
