<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Comment extends Model
{
    protected $table = 'comments';
    protected $fillable = ['id', 'content_id', 'user_id', 'body'];

    // geeft de eigenaar van een comment
    public function owner()
    {
        return $this->belongsTo('App\User');
    }

    // geeft het content object waartoe de comment behoort.
    public function post()
    {
        return $this->belongsTo('App\Project','project_id');
    }
}
