@extends('layouts.app')

@section('content')
@if (session('added'))
    <div class="alert alert-danger">
        {{ session('added') }}
    </div>
@endif
<div class="container">
    <div class="row">
    	<div class="col-xs-0 col-md-2"></div>
    	<div id="test" class="col-xs-12 col-md-8">
	        <h1>Content toevoegen</h1>
			{{ Form::open(array('url' => 'content/add')) }}
			    {{ Form::label('url', 'URL') }}
	    		{{ Form::text('url', '') }}
	    		{!! Form::submit("Toevoegen", array('id' =>'submitContent', 'class'=>'send-btn btn btn-primary upload-cta')) !!}
			{{ Form::close() }}
			@if (count($errors) > 0)
		    <div class="alert alert-danger">
		        <ul>
		            @foreach ($errors->all() as $error)
		                <li>{{ $error }}</li>
		            @endforeach
		        </ul>
		    </div>
			@endif
		</div>
		<div class="col-xs-0 col-md-2"></div>
    </div>
</div>
@endsection