@extends('layouts.app')

@section('content')
@if (session('added'))
    <div class="alert alert-success">
        {{ session('added') }}
    </div>
@endif
<div class="container spark-screen">
    <div class="row">
    	@if($content)
    	@foreach ($content as $c)
		    <h4>{{{ $c->titel }}}</h4>
		    <br>
		    @if($c->type !== "other")
			    <div class="media">
			        <div class="media-body">
			            <iframe width="560" height="315" src="{{{$c->embed}}}" frameborder="0" allowfullscreen>
			            </iframe>
			        </div>
			    </div>
			    <br>
		    @else
				<img class="image_content" src="{{{$c->embed}}}">
		    @endif
		@endforeach
		@endif
    </div>
</div>
@endsection
